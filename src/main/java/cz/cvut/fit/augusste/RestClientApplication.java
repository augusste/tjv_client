package cz.cvut.fit.augusste;

import cz.cvut.fit.augusste.DTO.CompetitionCreateDTO;
import cz.cvut.fit.augusste.DTO.CompetitionDTO;
import cz.cvut.fit.augusste.DTO.PlayerCreateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.client.RestTemplateCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.hateoas.config.EnableHypermediaSupport;
import org.springframework.hateoas.config.HypermediaRestTemplateConfigurer;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;



@SpringBootApplication
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class RestClientApplication implements ApplicationRunner {

    @Autowired
    CompetitionResource competitionResource;
    PlayerResource playerResource;

    public static void main(String[] args) {
        SpringApplication.run(RestClientApplication.class, args);
    }

    @Bean
    RestTemplateCustomizer hypermediaRestTemplateCustomizer(HypermediaRestTemplateConfigurer configurer) {
        return restTemplate -> {
            configurer.registerHypermediaTypes(restTemplate);
        };
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        if (args.containsOption("app.action") && args.containsOption("app.entity")) {
            switch (args.getOptionValues("app.entity").get(0)) {
                case "Competition":
                    switch (args.getOptionValues("app.action").get(0)) {
//                        case "readOne": {
//                            if (!args.containsOption("app.firstName")) {
//                                System.err.println("app.firstName missing");
//                                return;
//                            }
//                            String id = args.getOptionValues("app.firstName").get(0);
//                            try {
//                                String lastName = characterResource.readOne(id).getLastName();
//                                System.out.println(lastName);
//                            } catch (HttpClientErrorException e) {
//                                if (e.getStatusCode().equals(HttpStatus.NOT_FOUND))
//                                    System.err.println("Not found");
//                            }
//                        }
//                        break;
//                        case "update": {
//                            if (checkNameOptions(args)) return;
//                            String id = args.getOptionValues("app.firstName").get(0);
//                            String lastName = args.getOptionValues("app.lastName").get(0);
//                            CharacterModel data = new CharacterModel(id, lastName, null);
//                            characterResource.update(data);
//                        }
//                        break;
                        case "create": {
                            if (checkNameOptions(args)) return;
                            String name = args.getOptionValues("app.name").get(0);
                            String season = args.getOptionValues("app.season").get(0);
                            CompetitionCreateDTO data = new CompetitionCreateDTO(name, season);
                            try {
                                System.out.println(competitionResource.create(data));
                            } catch (HttpClientErrorException e) {
                                if (e.getStatusCode() == HttpStatus.CONFLICT)
                                    System.err.println("Already exists");
                            }
                        }
                        break;

                    }
                case "Player":{
                    switch (args.getOptionValues("app.action").get(0)) {
                        case "create": {
                            if (checkNameOptions(args)) return;
                            String first_name = args.getOptionValues("app.first_name").get(0);
                            String surname = args.getOptionValues("app.surname").get(0);
                            String ageString = args.getOptionValues("app.age").get(0);
                            int age = Integer.parseInt(ageString);
                            String post = args.getOptionValues("app.post").get(0);
                            PlayerCreateDTO player = new PlayerCreateDTO(first_name, surname, age, post, null);
                            try {
                                System.out.println(playerResource.create(player));
                            } catch (HttpClientErrorException e) {
                                if (e.getStatusCode() == HttpStatus.CONFLICT)
                                    System.err.println("Already exists");
                            }
                        }
                        case "update": {


                        }

                    }

                }
            }
        }
    }

    private static boolean checkNameOptions(ApplicationArguments args) {
        if (!args.containsOption("app.firstName") || !args.containsOption("app.lastName")) {
            System.err.println("some values missing");
            return true;
        }
        return false;
    }
    }
}
