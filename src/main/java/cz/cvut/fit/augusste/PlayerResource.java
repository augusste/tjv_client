package cz.cvut.fit.augusste;

import cz.cvut.fit.augusste.DTO.CompetitionCreateDTO;
import cz.cvut.fit.augusste.DTO.CompetitionDTO;
import cz.cvut.fit.augusste.DTO.PlayerCreateDTO;
import cz.cvut.fit.augusste.DTO.PlayerDTO;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class PlayerResource {

    private final RestTemplate restTemplate;

    private static final String ROOT_RESOURCE_URL = "http://localhost:8080/api/v1/player";
    private static final String ONE_URI = "/{id}";

    public PlayerResource(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).build();
    }

    public URI create(PlayerCreateDTO data)
    {
        return restTemplate.postForLocation(ROOT_RESOURCE_URL, data);
    }

    public URI update(int player_id, PlayerCreateDTO data)
    {
        return restTemplate.postForLocation(ROOT_RESOURCE_URL+ONE_URI, player_id, data);
    }

    public void delete(int player_id)
    {
        restTemplate.delete(ROOT_RESOURCE_URL+ONE_URI, player_id);
    }

    public PlayerDTO read(int player_id)
    {
        return restTemplate.getForObject(ROOT_RESOURCE_URL+ONE_URI, PlayerDTO.class, player_id);
    }

}