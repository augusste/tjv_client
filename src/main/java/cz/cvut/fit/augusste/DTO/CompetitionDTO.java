package cz.cvut.fit.augusste.DTO;

public class CompetitionDTO {


    private final int competition_id;
    private final String name;
    private final String season;

    public CompetitionDTO(int competition_id,String name, String season) {
        this.competition_id = competition_id;
        this.name = name;
        this.season = season;
    }

    public int getCompetition_id() {
        return competition_id;
    }

    public String getName() {
        return name;
    }

    public String getSeason() {
        return season;
    }
}
