package cz.cvut.fit.augusste.DTO;

import com.sun.istack.NotNull;
import cz.cvut.fit.augusste.entity.Match;

import javax.persistence.*;
import java.util.List;


public class TeamDTO {


    private final int team_id;

    private final String name;

    private final String city;

    private final List<Integer> matches;

    public TeamDTO(int team_id, String name, String city, List<Integer> matches) {
        this.team_id = team_id;
        this.name = name;
        this.city = city;
        this.matches = matches;
    }

    public int getTeam_id() {
        return team_id;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public List<Integer> getMatches() {
        return matches;
    }
}
