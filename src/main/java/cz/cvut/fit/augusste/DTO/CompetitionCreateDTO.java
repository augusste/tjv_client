package cz.cvut.fit.augusste.DTO;

public class CompetitionCreateDTO {


    private final String name;
    private final String season;

    public CompetitionCreateDTO(String name, String season) {
        this.name = name;
        this.season = season;
    }

    public String getName() {
        return name;
    }

    public String getSeason() {
        return season;
    }
}
