package cz.cvut.fit.augusste.DTO;

public class PlayerDTO {

    private final int player_id;

    private final String first_name;

    private final String surname;

    private final int age;

    private final String post;

    private final Integer team;

    public PlayerDTO(int player_id, String first_name, String surname, int age, String post, Integer team) {
        this.player_id = player_id;
        this.first_name = first_name;
        this.surname = surname;
        this.age = age;
        this.post = post;
        this.team = team;
    }

    public int getPlayer_id() {
        return player_id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public String getPost() {
        return post;
    }

    public Integer getTeam() {
        return team;
    }
}
