package cz.cvut.fit.augusste.DTO;

import cz.cvut.fit.augusste.entity.Match;

import java.util.List;


public class TeamCreateDTO {

    private final String name;

    private final String city;

    private final List<Integer> matches;

    public TeamCreateDTO(String name, String city, List<Integer> matches) {
        this.name = name;
        this.city = city;
        this.matches = matches;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public List<Integer> getMatches() {
        return matches;
    }
}
