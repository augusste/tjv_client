package cz.cvut.fit.augusste.DTO;


public class MatchDTO {


    private final int match_id;

    private final int winner;

    private final int loser;

    private final int set_winner;

    private final int set_loser;


    private final Integer competition;

    public MatchDTO(int match_id, int winner, int loser, int set_winner, int set_loser, Integer competition) {
        this.match_id = match_id;
        this.winner = winner;
        this.loser = loser;
        this.set_winner = set_winner;
        this.set_loser = set_loser;
        this.competition = competition;
    }

    public int getMatch_id() {
        return match_id;
    }

    public int getWinner() {
        return winner;
    }

    public int getLoser() {
        return loser;
    }

    public int getSet_winner() {
        return set_winner;
    }

    public int getSet_loser() {
        return set_loser;
    }

    public Integer getCompetition() {
        return competition;
    }
}
