package cz.cvut.fit.augusste.DTO;


public class MatchCreateDTO {


    private final int winner;

    private final int loser;

    private final int set_winner;

    private final int set_loser;

    private final Integer competition;

    public MatchCreateDTO(int winner, int loser, int set_winner, int set_loser, Integer competition) {
        this.winner = winner;
        this.loser = loser;
        this.set_winner = set_winner;
        this.set_loser = set_loser;
        this.competition = competition;
    }

    public int getWinner() {
        return winner;
    }

    public int getLoser() {
        return loser;
    }

    public int getSet_winner() {
        return set_winner;
    }

    public int getSet_loser() {
        return set_loser;
    }

    public Integer getCompetition() {
        return competition;
    }
}
