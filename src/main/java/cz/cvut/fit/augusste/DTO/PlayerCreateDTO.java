package cz.cvut.fit.augusste.DTO;

public class PlayerCreateDTO {

    private final String first_name;

    private final String surname;

    private final int age;

    private final String post;

    private final Integer team;

    public PlayerCreateDTO(String first_name, String surname, int age, String post, Integer team) {
        this.first_name = first_name;
        this.surname = surname;
        this.age = age;
        this.post = post;
        this.team = team;
    }


    public String getFirst_name() {
        return first_name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public String getPost() {
        return post;
    }

    public Integer getTeam() {
        return team;
    }
}
