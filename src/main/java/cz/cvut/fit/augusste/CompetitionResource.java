package cz.cvut.fit.augusste;


import cz.cvut.fit.augusste.DTO.CompetitionCreateDTO;
import cz.cvut.fit.augusste.DTO.CompetitionDTO;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.net.URI;

@Component
public class CompetitionResource {

    private final RestTemplate restTemplate;

    private static final String ROOT_RESOURCE_URL = "http://localhost:8080/api/v1/competition";
    private static final String ONE_URI = "/{id}";

    public CompetitionResource(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.rootUri(ROOT_RESOURCE_URL).build();
    }

    public URI create(CompetitionCreateDTO data)
    {
        return restTemplate.postForLocation(ROOT_RESOURCE_URL, data);
    }

}
